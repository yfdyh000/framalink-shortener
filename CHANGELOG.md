# 2.2.2
* i: Message on request error

# 2.2.1
* i: Clipboard copy (Firefox only)

# 2.2.0
* i: Clipboard without using content script (Chrome/Chromium only, currently)

# 2.1.0
* i: Add menu item to tab context menu when supported (Firefox only, currently)

# 2.0.0
* Fix: Possibility to lose preferences because of the preference storage
* i: Code rework to add the possibility to copy the text of the links (unused for now)

# 1.2.0
* Fix: Fixes to make Web Extension working with Firefox

# 1.1.1
* Fix: Remove console.* for mozilla build

# 1.1.0
* +: Support img tags
* Fix: Context menu for links

# 1.0.0
* +: Action button
* +: Context menu to shorten the page
* +: Addon icon

# 0.9.1
* i: Localization changes
* Fix: Description localization

# 0.9.0
* i: Initial version
